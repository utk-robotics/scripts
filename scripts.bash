
### install location detection
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

echo "adding $DIR to path..."
PATH="${PATH}:${DIR}/bin"
