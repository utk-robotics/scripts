
### install location detection
_SCRIPTPATH="${(%):-%N}"
_INSTALL_LOC="$(readlink -f $_SCRIPTPATH)"
_INSTALL_DIR="${_INSTALL_LOC%/*}"

PATH="${PATH}:${_INSTALL_DIR}/bin"

rehash
