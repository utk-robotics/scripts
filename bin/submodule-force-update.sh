#!/usr/bin/env zsh

set -e

set -v
git submodule sync --recursive
git fetch --recurse-submodules
git submodule update --init --recursive --force
[[ -d scripts ]] && {
  git submodule update --init --recursive --remote scripts
}

