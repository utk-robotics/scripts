#!/usr/bin/env zsh

[[ -d scripts ]] || {
  echo Please use this in a project repo with scripts as a submodule. >&2
  exit 1
}

ln -s "${@}" scripts/{build.sh,.clang-format,.gitignore} ./ || {
  echo "Some links cannot be created, you should check these yourself for accuracy."
}

[[ -e .gitlab-ci.yml ]] || {
  echo Copying .gitlab-ci.yml
  cp scripts/.gitlab-ci.yml ./
}

