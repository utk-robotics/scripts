#!/bin/bash

python "$(dirname "$0" )"/autoheader.py "$*"

exit $?

# TODO auto apply to multiple headers
# run python for each file input

if ! command -v python ; then
  echo "Please install python!"
fi

if [ -z "$NO_CD" ]; then
  # push into own dir
  pushd "$(dirname "$0" )"
  pushd ..
fi
