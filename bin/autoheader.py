#!/usr/bin/env python
import sys
import shutil
import argparse

argparser = argparse.ArgumentParser()
argparser.add_argument(
    "file",
    help="Path to file to apply automatic header guards to."
)
argparser.add_argument(
    "-f", "--force",
    help="Force headerguard application.",
    action="store_true"
)
argparser.add_argument(
    "-n", "--dry-run",
    help="Don't actually change the file.",
    action="store_true"
)
argparser.add_argument(
    "-s", "--showfile",
    help="Show contents of modified file.",
    action="store_true"
)
args = argparser.parse_args()

file_path = str(args.file)

if file_path[-2:] != ".h" and \
    file_path[-4:] != ".hpp" and \
    args.force is not True:
    print "Are you sure this is a C++ header?"
    raw_input("Press enter to continue anyways, Ctrl-C to abort:")

linelist = open(file_path).readlines()

guard = file_path.upper().replace('/','_').replace('.','_')
if guard.startswith("INCLUDE_"): guard = guard.replace("INCLUDE_","")

print "Guard: " + guard

if not linelist[0].startswith("#ifndef") or not linelist[1].startswith("#define"):
    print "No previous #ifndef/#define combo found..."
    raw_input("Press enter to confirm ADDING header guards:")
    linelist.insert(0, "#ifndef TMP_SCRIPT_UNFINISHED\n")
    linelist.insert(1, "#define TMP_SCRIPT_UNFINISHED\n")
    if len(linelist[2]) == 1 and len(linelist[3]) == 1:
        del linelist[2]
    if linelist[-1] != "\n":
        linelist.append("\n")
    linelist.append("#endif // TMP_SCRIPT_UNFINISHED")
# else we will override the old guards

linelist[0] = "#ifndef " + guard + "\n"
linelist[1] = "#define " + guard + "\n"
linelist[-1] = "#endif // " + guard + "\n"

if args.showfile:
    print ''.join(linelist)

if args.dry_run:
    print "Not writing file, --dry-run in use."
    exit(0)
else:
    with open(file_path, 'w') as file:
        print "Writing " + file_path
        for line in linelist:
            file.write(line)
        file.flush()
