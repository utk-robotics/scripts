import sys
import argparse
import CppHeaderParser

'''
This will do most of the busy work for taking an arduino library and writing
it into an embedMessenger format.
'''
argparser = argparse.ArgumentParser()
argparser.add_argument(
    "in_file",
    help="input header :)"
)
argparser.add_argument(
    "out_file_hpp",
    help="hpp output"
)
argparser.add_argument(
    "out_file_cpp",
    help="cpp output"
)
argparser.add_argument(
    "class_to_parse",
    help="Names of class to parse."
)

args = argparser.parse_args()
in_path = str(args.in_file)

if in_path[-2:] != ".h" and \
    in_path[-4:] != ".hpp":
    print("Are you sure class is a C++ header?")
    raw_input("Press enter to continue anyways, Ctrl-C to abort:")

try:
    cppHeader = CppHeaderParser.CppHeader(in_path)
except CppHeaderParser.CppParseError as e:
    print(e)
    sys.exit(1)

cl = str(args.class_to_parse)

if args.class_to_parse not in cppHeader.classes:
	print(args.class_to_parse + " not found in header file.\n")
	print("classes in header file:")
	print(cppHeader.classes.keys())
	sys.exit(1)

hpp_out = open(str(args.out_file_hpp), "w")
cpp_out = open(str(args.out_file_cpp), "w")
out_file_hpp = str(args.out_file_hpp)

# Write header guards
out_file_hpp = out_file_hpp.split('/')[-1]
out_file_hpp = out_file_hpp.upper()
out_file_hpp = out_file_hpp.replace('.', '_')
header_guard = "#ifndef {}\n#define {}".format(out_file_hpp, out_file_hpp)
hpp_out.write(header_guard)
hpp_out.write('\n')

#  Write includes
for inc in cppHeader.includes:
	hpp_out.write("#include {}\n".format(inc))

hpp_out.write("#include <EmbMessenger/Command.hpp>\n")
hpp_out.write("#include <EmbMessenger/EmbMessenger.hpp>\n")

cpp_out.write("#include \"{}\"\n".format(args.out_file_hpp))
cpp_out.write('\n')
hpp_out.write('\n')

# Write defines (currently writes a define for the class its copying from...)
for dfn in cppHeader.defines:
	hpp_out.write("#define {}\n".format(dfn))

for func in cppHeader.classes[cl]["methods"]["public"]:
	hpp_out.write("class {} : public emb::Command\n{{\n".format(func["name"]))

	# write parameters
	parameters = []
	for parameter in func['parameters']:
		hpp_out.write(parameter['type'] + ' ' + 'm_' + parameter['name'] + ';\n')
		parameters.append([parameter['type'], parameter['name']])

	if len(parameters) > 0:

		hpp_out.write("// Parameter members\n")

		if func['returns'] != 'void':
			hpp_out.write(func['returns'] + ' ' + 'm_rv;\n')

		# write constructor
		hpp_out.write("public:\n")

		hpp_out.write(func["name"] + '(')
		cpp_out.write(func['name'] + '::' + func['name'] + '(')

		constructor_initialization = ": "
		for p_pair in parameters[:-2]:
			hpp_out.write(p_pair[0] + ' ' + p_pair[1] + ',')
			cpp_out.write(p_pair[0] + ' ' + p_pair[1] + ',')
			constructor_initialization += 'm_' + p_pair[1] + '({})'.format(p_pair[1]) + ', '

		hpp_out.write(parameters[-1][0] + ' ' + parameters[-1][1] + ');\n')
		cpp_out.write(parameters[-1][0] + ' ' + parameters[-1][1] + ')')
		constructor_initialization += 'm_' + parameters[-1][1] + '({})'.format(parameters[-1][1])
		cpp_out.write(' {}'.format(constructor_initialization))

		cpp_out.write('{}\n')
		cpp_out.write('\n')

		# write send &/| receive
		hpp_out.write('void send(emb::EmbMessenger* messenger);\n')
		cpp_out.write(func['name'] + '::send(emb::EmbMessenger* messenger)\n{\n')
		cpp_out.write('messenger->write(')

		for p_pair in parameters[:-2]:
			cpp_out.write('m_' + p_pair[1] + ', ')

		cpp_out.write('m_' + parameters[-1][1] + ');\n}\n')
		cpp_out.write('\n')

		if func['returns'] != 'void':
			hpp_out.write('void receive(emb::EmbMessenger* messenger);\n')
			cpp_out.write(func['name'] + '::receive(emb::EmbMessenger* messenger)\n{\n')
			cpp_out.write('messenger->read(m_rv);\n}\n')
			cpp_out.write('\n')

		# Write getters
		for p_pair in parameters:
			temp = p_pair[1][0].upper() + p_pair[1][1:]
			hpp_out.write(p_pair[0] + ' ' + 'get' + temp +'() const;\n')
	else:
		print('function {} not written. Reason: No parameters.'.format(func['name']))

	hpp_out.write('};\n\n')

hpp_out.write("#endif")
hpp_out.close()
cpp_out.close()

